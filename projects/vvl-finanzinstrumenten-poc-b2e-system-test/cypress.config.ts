import { defineConfig } from 'cypress';

export default defineConfig({
    chromeWebSecurity: false,
    fileServerFolder: '.',
    fixturesFolder: './fixtures',
    reporter: 'junit',
    reporterOptions: {
        mochaFile: '../../test-results/report.xml',
        toConsole: true,
    },
    screenshotsFolder: '../../dist/system-test/screenshot',
    video: true,
    viewportWidth: 1600,
    viewportHeight: 768,
    videosFolder: '../../dist/system-test/videos',
    e2e: {
        setupNodeEvents(
            on: Cypress.PluginEvents,
            config: Cypress.PluginConfigOptions,
        ): Promise<Cypress.PluginConfigOptions | void> | Cypress.PluginConfigOptions | void {
            // configure plugins here
        },
        baseUrl: 'http://localhost:4200',
        specPattern: './integration/**/*.cy.{js,jsx,ts,tsx}',
        supportFile: './support/index.ts',
    },
});
