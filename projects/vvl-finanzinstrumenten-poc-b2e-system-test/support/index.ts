// ***********************************************************
// This example support/index.js is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************
beforeEach(() => {
    cy.request('/.auth/me').then(({ body }) => {
        const accessToken = body[0].accessToken;
        if (accessToken) {
            cy.log('Access token found and set in the session storage!');
        } else {
            cy.log('No access token found, please log on first!');
        }
    });
});
