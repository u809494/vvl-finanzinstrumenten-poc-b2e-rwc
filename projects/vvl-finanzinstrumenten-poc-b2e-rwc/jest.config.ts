import type { Config } from '@jest/types';

const config: Config.InitialOptions = {
    preset: 'jest-preset-angular',
    setupFilesAfterEnv: ['<rootDir>/jest.setup.ts'],
    transformIgnorePatterns: ['node_modules/(?!.*\\.mjs$)'],
    moduleNameMapper: { '^raw-loader!(.*)$': '<rootDir>/jest.mocks.ts' },
    maxWorkers: 2,

    // coverage & test reporting
    coverageDirectory: '../../coverage',
    coverageReporters: ['html', 'text-summary', 'cobertura'],
    collectCoverageFrom: ['**/src/{lib,app}/**/*.{ts,js}', '!**/public{_,-}api.ts', '!**/index.ts', '!**/*.module.ts'],
    reporters: ['default', ['jest-junit', { outputDirectory: 'test-results' }]],
};
export default config;
