import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { TranslationModule } from '@mobi/rwc-utils-ng-jslib';
import { HeadingModule, LayoutModule, SectionModule, ButtonModule } from '@mobi/rwc-uxframework-ng';

@NgModule({
    declarations: [],
    imports: [
        // vendor
        CommonModule,

        // uxframework
        HeadingModule,
        SectionModule,
        LayoutModule,
        ButtonModule,

        // rwc-utils
        TranslationModule,
    ],
    exports: [
        // vendor
        CommonModule,

        // uxframework
        HeadingModule,
        SectionModule,
        LayoutModule,
        ButtonModule,

        // rwc-utils
        TranslationModule,

        // local
    ],
})
export class SharedModule {}
