import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';

import { B2eAzureCoreModule } from '@mobi/b2e-utils-ng-jslib';
import { LayoutModule, Layout131Module, HeadingModule, MenuModule, TileModule } from '@mobi/rwc-uxframework-ng';

import { MainLayoutComponent } from './layout/main-layout/main-layout.component';

@NgModule({
    declarations: [MainLayoutComponent],
    imports: [
        // vendor
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        RouterModule,

        // uxframework
        Layout131Module,
        HeadingModule,
        MenuModule,
        LayoutModule,
        TileModule,

        // b2e-utils
        B2eAzureCoreModule,
    ],
    exports: [MainLayoutComponent],
})
export class CoreModule {}
