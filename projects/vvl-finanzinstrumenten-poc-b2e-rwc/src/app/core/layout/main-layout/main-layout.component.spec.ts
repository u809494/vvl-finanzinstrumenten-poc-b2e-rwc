import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { B2eStarterPortalheaderModule } from '@mobi/b2e-utils-ng-jslib';
import { ConfigurationService, UserSessionService } from '@mobi/rwc-utils-ng-jslib';
import { mockConfigurationService, mockUserSessionService } from '@mobi/rwc-utils-ng-jslib/testing';
import { LayoutModule, MenuModule, Layout131Module } from '@mobi/rwc-uxframework-ng';

import { MainLayoutComponent } from './main-layout.component';

describe('MainLayoutComponent', () => {
    let component: MainLayoutComponent;
    let fixture: ComponentFixture<MainLayoutComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [MainLayoutComponent],
            imports: [
                B2eStarterPortalheaderModule,
                LayoutModule,
                MenuModule,
                Layout131Module,
                RouterTestingModule,
                HttpClientTestingModule,
            ],
            providers: [
                { provide: ConfigurationService, useValue: mockConfigurationService() },
                { provide: UserSessionService, useValue: mockUserSessionService() },
            ],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(MainLayoutComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
