import { Component, ChangeDetectionStrategy } from '@angular/core';

import { BusinessContext } from '@mobi/rwc-uxframework-ng';

@Component({
    selector: 'app-main-layout',
    templateUrl: './main-layout.component.html',
    styleUrls: ['./main-layout.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MainLayoutComponent {
    businessContext: typeof BusinessContext = BusinessContext;
}
