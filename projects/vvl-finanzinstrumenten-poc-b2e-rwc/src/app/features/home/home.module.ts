import { NgModule } from '@angular/core';

import { mobiIconLighthouse, mobiIconBulb } from '@mobi/rwc-utils-icons-jslib';
import { B2eSplitButtonModule, IconsModule, MobiIconRegistry } from '@mobi/rwc-uxframework-ng';

import { SharedModule } from '../../shared/shared.module';

import { GraphComponent } from '../graph/graph.component';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';

@NgModule({
    declarations: [HomeComponent, GraphComponent],
    imports: [SharedModule, HomeRoutingModule, IconsModule, B2eSplitButtonModule],
})
export class HomeModule {
    constructor(iconRegistry: MobiIconRegistry) {
        // icons are registered per lazy loaded module (treeshakeable icons)
        iconRegistry.registerIcons([mobiIconBulb, mobiIconLighthouse]);
    }
}
