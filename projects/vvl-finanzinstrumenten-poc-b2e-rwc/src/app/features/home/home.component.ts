import { DOCUMENT } from '@angular/common';
import { Component, ChangeDetectionStrategy, Inject } from '@angular/core';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeComponent {
    constructor(@Inject(DOCUMENT) private document: Document) {}

    goToLink(url: string): void {
        this.document.location.href = url;
    }
}
