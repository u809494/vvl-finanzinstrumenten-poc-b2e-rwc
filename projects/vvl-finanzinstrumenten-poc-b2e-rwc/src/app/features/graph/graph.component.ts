import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
// TODO bundle size importer module par module plutot
import * as d3 from 'd3';
import { DatePipe } from '@angular/common';

export interface Data {
    valueDate: Date;
    price: number;
}

export enum XRange {
    ONE_YEAR = '1Y',
    THREE_YEAR = '3Y',
    FIVE_YEAR = '5Y',
    YEAR_TO_DATE = 'YTD',
    ALL = 'All',
}

export interface Margin {
    top: number;
    bottom: number;
    left: number;
    right: number;
}

@Component({
    selector: 'app-graph',
    templateUrl: './graph.component.html',
    styleUrls: ['./graph.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GraphComponent implements OnInit {
    private readonly data: Data[] = mock().map(mock => {
        return { ...mock, valueDate: new Date(mock.valueDate) };
    });
    private readonly width: number = 928;
    private readonly height: number = 500;
    private readonly areaHeight: number = 100;
    private readonly margin: Margin = { top: 20, bottom: 30, right: 30, left: 40 };
    private readonly percentMarginY = 10;

    private readonly minX = d3.min(this.data, d => d.valueDate)!;
    readonly maxX = d3.max(this.data, d => d.valueDate)!;
    private readonly minY = this.withMarginYMin(d3.min(this.data, d => d.price)!);
    private readonly maxY = this.withMarginYMax(d3.max(this.data, d => d.price)!);

    private readonly recordX: Record<XRange, Date[]> = {
        [XRange.ALL]: [this.minX, this.maxX],
        [XRange.ONE_YEAR]: [this.getMinX(12), this.maxX],
        [XRange.THREE_YEAR]: [this.getMinX(36), this.maxX],
        [XRange.FIVE_YEAR]: [this.getMinX(60), this.maxX],
        [XRange.YEAR_TO_DATE]: [this.getMinXYtd(), this.maxX],
    };

    readonly XRange = XRange;
    selectedXRange: string = XRange.ALL;

    x: d3.ScaleTime<number, number> = d3.scaleUtc(
        [this.minX, this.maxX],
        [this.margin.left, this.width - this.margin.right],
    );
    x2: d3.ScaleTime<number, number> = this.x.copy();

    y: d3.ScaleLinear<number, number> = d3.scaleLinear(
        [this.minY, this.maxY],
        [this.height - this.margin.bottom, this.margin.top],
    );
    y2: d3.ScaleLinear<number, number> = d3.scaleLinear(
        [this.minY, this.maxY],
        [this.areaHeight - this.margin.bottom, this.margin.top],
    );
    line: d3.Line<Data> = d3
        .line(
            (d: Data) => this.x(d.valueDate),
            (d: Data) => this.y(d.price),
        )
        .curve(d3.curveMonotoneX);

    area: d3.Area<Data> = d3
        .area<Data>()
        .x(d => this.x2(d.valueDate))
        .y0(this.y2(0))
        .y1(d => this.y2(d.price));

    bisector = d3.bisector<Data, Date>((d: Data, x: Date) => x.getTime() - d.valueDate.getTime());

    // TODO to type
    gX: any;
    gY: any;
    gBrush: any;
    gPath: any;
    gTooltip: any;
    brush: any;

    constructor(private readonly datePipe: DatePipe) {}

    ngOnInit(): void {
        this.buildLineChart();
        this.buildAreaChart();
    }

    handleXRangeChange(xRange: string) {
        this.selectedXRange = xRange;
        this.gBrush.call(this.brush.move, this.recordX[xRange as XRange].map(this.x2));
    }

    private buildAreaChart(): void {
        // Create the SVG container.
        const svg = d3
            .select('figure#area')
            .append('svg')
            .attr('width', this.width)
            .attr('height', this.areaHeight)
            .attr('viewBox', [0, 0, this.width, this.areaHeight])
            .attr('style', 'max-width: 100%; height: auto; height: intrinsic;');

        // Append a path for the area (under the axes).
        svg.append('path').attr('fill', 'steelblue').attr('d', this.area(this.data));

        this.brush = d3
            .brushX()
            .extent([
                [this.margin.left, 0.5],
                [this.width - this.margin.right, this.areaHeight + 0.5],
            ])
            .on('brush', ({ selection }: any) => {
                // TODO perf issue ? a déplacer dans 'end' ?
                if (selection) {
                    const dateExtends = selection.map(this.x2.invert, this.x2).map(d3.utcDay.round);
                    const filteredData = this.data.filter(
                        d => d.valueDate >= dateExtends[0] && d.valueDate <= dateExtends[1],
                    );
                    this.x.domain(dateExtends);
                    this.y.domain([
                        this.withMarginYMin(d3.min(filteredData, d => d.price) ?? 0),
                        this.withMarginYMax(d3.max(filteredData, d => d.price) ?? 0),
                    ]);

                    this.updateXAxis();
                    this.updateYAxis();
                    this.gPath.attr('d', this.line(filteredData));
                }
            })
            .on('end', ({ selection }: any) => {
                if (!selection) {
                    this.gBrush.call(this.brush.move, this.x2.range());
                }
            });

        this.gBrush = svg.append('g').call(this.brush).call(this.brush.move, this.x2.range());
    }

    private buildLineChart(): void {
        const svg = d3
            .select('figure#line')
            .append('svg')
            .attr('width', this.width)
            .attr('height', this.height)
            .attr('viewBox', [0, 0, this.width, this.height])
            .attr('style', 'max-width: 100%; height: auto; height: intrinsic;')
            .on('pointerenter pointermove', e => this.pointerMoved(e))
            .on('pointerleave', () => this.pointerLeft())
            .on('touchstart', e => e.preventDefault());

        // Add the x-axis.
        this.gX = svg.append('g').attr('transform', `translate(0,${this.height - this.margin.bottom})`);
        this.updateXAxis();

        // Add the y-axis, remove the domain line, add grid lines and a label.
        this.gY = svg.append('g').attr('transform', `translate(${this.margin.left},0)`);
        this.updateYAxis()
            .call((g: any) => g.select('.domain').remove())
            .call((g: any) =>
                g
                    .selectAll('.tick line')
                    .clone()
                    .attr('x2', this.width - this.margin.left - this.margin.right)
                    .attr('stroke-opacity', 0.1),
            )
            .call((g: any) =>
                g
                    .append('text')
                    .attr('x', -this.margin.left)
                    .attr('y', 10)
                    .attr('fill', 'currentColor')
                    .attr('text-anchor', 'start')
                    .text('↑ Value (CHF)'),
            );

        // Append a path for the line.
        this.gPath = svg
            .append('path')
            .attr('fill', 'none')
            .attr('stroke', 'steelblue')
            .attr('stroke-width', 1.5)
            // TODO updatePath() ?
            .attr('d', this.line(this.data));

        this.gTooltip = svg.append('g');
    }

    private getMinX(minusMonth: number): Date {
        const min = new Date();
        if (minusMonth) {
            min.setMonth(min.getMonth() - minusMonth);
        }
        return min >= this.minX ? min : this.minX;
    }

    private getMinXYtd(): Date {
        return new Date(new Date().getFullYear(), 0, 1);
    }

    private updateXAxis(): any {
        // TODO use transition for fluid update ?
        return this.gX.call(
            d3
                .axisBottom(this.x)
                .ticks(this.width / 80)
                .tickSizeOuter(0),
        );
    }

    private updateYAxis(): any {
        // TODO use transition for fluid update ?
        return this.gY.call(d3.axisLeft(this.y).ticks(this.height / 40));
    }

    private pointerLeft(): void {
        this.gTooltip.style('display', 'none');
    }

    private pointerMoved(event: PointerEvent): void {
        const i = this.bisector.center(this.data, this.x.invert(d3.pointer(event)[0]));

        this.gTooltip.style('display', null);
        this.gTooltip.attr(
            'transform',
            `translate(${this.x(new Date(this.data[i].valueDate))},${this.y(this.data[i].price)})`,
        );

        const path = this.gTooltip
            .selectAll('path')
            .data([,])
            .join('path')
            .attr('fill', 'white')
            .attr('stroke', 'black');

        const text = this.gTooltip
            .selectAll('text')
            .data([,])
            .join('text')
            .call((text: any) =>
                text
                    .selectAll('tspan')
                    .data([this.datePipe.transform(this.data[i].valueDate), this.data[i].price])
                    .join('tspan')
                    .attr('x', 0)
                    .attr('y', (_: any, i: any) => `${i * 1.1}em`)
                    .attr('font-weight', (_: any, i: any) => (i ? null : 'bold'))
                    .text((d: any) => d),
            );

        this.size(text, path);
    }

    private size(text: any, path: any) {
        const { x, y, width: w, height: h } = text.node().getBBox();
        text.attr('transform', `translate(${-w / 2},${15 - y})`);
        path.attr('d', `M${-w / 2 - 10},5H-5l5,-5l5,5H${w / 2 + 10}v${h + 20}h-${w + 20}z`);
    }

    private withMarginYMin(num: number) {
        return num - this.calculatePercentage(num);
    }
    private withMarginYMax(num: number) {
        return num + this.calculatePercentage(num);
    }
    private calculatePercentage(num: number) {
        return (this.percentMarginY / 100) * num;
    }
}

// TODO load all data in one call ? pageable service ?
export function mock(): any[] {
    return [
        {
            id: 37513,
            valueDate: '2023-08-11',
            price: 832.8,
            createdAt: '2023-08-11T22:00:00Z',
            changedAt: '2023-08-13T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-08-11',
                },
            ],
        },
        {
            id: 37479,
            valueDate: '2023-08-10',
            price: 833.8,
            createdAt: '2023-08-10T22:00:00Z',
            changedAt: '2023-08-10T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-08-10',
                },
            ],
        },
        {
            id: 37445,
            valueDate: '2023-08-09',
            price: 833.4,
            createdAt: '2023-08-09T22:00:00Z',
            changedAt: '2023-08-09T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-08-09',
                },
            ],
        },
        {
            id: 37411,
            valueDate: '2023-08-08',
            price: 834.6,
            createdAt: '2023-08-08T22:00:00Z',
            changedAt: '2023-08-08T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-08-08',
                },
            ],
        },
        {
            id: 37377,
            valueDate: '2023-08-07',
            price: 836.4,
            createdAt: '2023-08-07T22:00:00Z',
            changedAt: '2023-08-07T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-08-07',
                },
            ],
        },
        {
            id: 37335,
            valueDate: '2023-08-04',
            price: 837.7,
            createdAt: '2023-08-04T22:00:00Z',
            changedAt: '2023-08-06T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-08-04',
                },
            ],
        },
        {
            id: 37301,
            valueDate: '2023-08-02',
            price: 839.2,
            createdAt: '2023-08-02T22:00:00Z',
            changedAt: '2023-08-03T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-08-02',
                },
            ],
        },
        {
            id: 37267,
            valueDate: '2023-07-31',
            price: 840.5,
            createdAt: '2023-07-31T22:00:00Z',
            changedAt: '2023-08-01T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-07-31',
                },
            ],
        },
        {
            id: 37233,
            valueDate: '2023-07-28',
            price: 840.6,
            createdAt: '2023-07-28T22:00:00Z',
            changedAt: '2023-07-30T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-07-28',
                },
            ],
        },
        {
            id: 37199,
            valueDate: '2023-07-27',
            price: 842.8,
            createdAt: '2023-07-27T22:00:00Z',
            changedAt: '2023-07-27T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-07-27',
                },
            ],
        },
        {
            id: 37165,
            valueDate: '2023-07-26',
            price: 843,
            createdAt: '2023-07-26T22:00:00Z',
            changedAt: '2023-07-26T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-07-26',
                },
            ],
        },
        {
            id: 37131,
            valueDate: '2023-07-25',
            price: 845.7,
            createdAt: '2023-07-25T22:00:00Z',
            changedAt: '2023-07-25T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-07-25',
                },
            ],
        },
        {
            id: 37097,
            valueDate: '2023-07-24',
            price: 844.5,
            createdAt: '2023-07-24T22:00:00Z',
            changedAt: '2023-07-24T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-07-24',
                },
            ],
        },
        {
            id: 37053,
            valueDate: '2023-07-21',
            price: 842.8,
            createdAt: '2023-07-21T22:00:00Z',
            changedAt: '2023-07-23T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-07-21',
                },
            ],
        },
        {
            id: 37019,
            valueDate: '2023-07-20',
            price: 842.2,
            createdAt: '2023-07-20T22:00:00Z',
            changedAt: '2023-07-20T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-07-20',
                },
            ],
        },
        {
            id: 36985,
            valueDate: '2023-07-19',
            price: 844.7,
            createdAt: '2023-07-19T22:00:00Z',
            changedAt: '2023-07-19T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-07-19',
                },
            ],
        },
        {
            id: 36951,
            valueDate: '2023-07-18',
            price: 846.4,
            createdAt: '2023-07-18T22:00:00Z',
            changedAt: '2023-07-18T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-07-18',
                },
            ],
        },
        {
            id: 36917,
            valueDate: '2023-07-17',
            price: 840.4,
            createdAt: '2023-07-17T22:00:00Z',
            changedAt: '2023-07-17T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-07-17',
                },
            ],
        },
        {
            id: 36865,
            valueDate: '2023-07-14',
            price: 839.8,
            createdAt: '2023-07-14T22:00:00Z',
            changedAt: '2023-07-16T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-07-14',
                },
            ],
        },
        {
            id: 36831,
            valueDate: '2023-07-13',
            price: 838.2,
            createdAt: '2023-07-13T22:00:00Z',
            changedAt: '2023-07-13T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-07-13',
                },
            ],
        },
        {
            id: 36797,
            valueDate: '2023-07-12',
            price: 831.8,
            createdAt: '2023-07-12T22:00:00Z',
            changedAt: '2023-07-12T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-07-12',
                },
            ],
        },
        {
            id: 36757,
            valueDate: '2023-07-10',
            price: 783.8,
            createdAt: '2023-07-10T22:00:00Z',
            changedAt: '2023-07-10T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-07-10',
                },
            ],
        },
        {
            id: 36710,
            valueDate: '2023-07-07',
            price: 786.3,
            createdAt: '2023-07-07T22:00:00Z',
            changedAt: '2023-07-09T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-07-07',
                },
            ],
        },
        {
            id: 36676,
            valueDate: '2023-07-05',
            price: 787.1,
            createdAt: '2023-07-05T22:00:00Z',
            changedAt: '2023-07-06T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-07-05',
                },
            ],
        },
        {
            id: 36643,
            valueDate: '2023-07-04',
            price: 841.8,
            createdAt: '2023-07-04T22:00:00Z',
            changedAt: '2023-07-04T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-07-04',
                },
            ],
        },
        {
            id: 36610,
            valueDate: '2023-07-03',
            price: 839.9,
            createdAt: '2023-07-03T22:00:00Z',
            changedAt: '2023-07-03T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-07-03',
                },
            ],
        },
        {
            id: 36577,
            valueDate: '2023-06-30',
            price: 838.4,
            createdAt: '2023-07-02T22:00:00Z',
            changedAt: '2023-07-02T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-06-30',
                },
            ],
        },
        {
            id: 36543,
            valueDate: '2023-06-29',
            price: 836,
            createdAt: '2023-06-29T22:00:00Z',
            changedAt: '2023-06-29T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-06-29',
                },
            ],
        },
        {
            id: 36510,
            valueDate: '2023-06-28',
            price: 841.5,
            createdAt: '2023-06-28T22:00:00Z',
            changedAt: '2023-06-28T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-06-28',
                },
            ],
        },
        {
            id: 36477,
            valueDate: '2023-06-27',
            price: 839.6,
            createdAt: '2023-06-27T22:00:00Z',
            changedAt: '2023-06-27T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-06-27',
                },
            ],
        },
        {
            id: 36435,
            valueDate: '2023-06-23',
            price: 836.5,
            createdAt: '2023-06-23T22:00:00Z',
            changedAt: '2023-06-25T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-06-23',
                },
            ],
        },
        {
            id: 36397,
            valueDate: '2023-06-21',
            price: 826.8,
            createdAt: '2023-06-21T22:00:00Z',
            changedAt: '2023-06-22T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-06-21',
                },
            ],
        },
        {
            id: 36366,
            valueDate: '2023-06-20',
            price: 829,
            createdAt: '2023-06-20T22:00:00Z',
            changedAt: '2023-06-20T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-06-20',
                },
            ],
        },
        {
            id: 36335,
            valueDate: '2023-06-19',
            price: 821,
            createdAt: '2023-06-19T22:00:00Z',
            changedAt: '2023-06-19T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-06-19',
                },
            ],
        },
        {
            id: 36295,
            valueDate: '2023-06-16',
            price: 819.5,
            createdAt: '2023-06-16T22:00:00Z',
            changedAt: '2023-06-18T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-06-16',
                },
            ],
        },
        {
            id: 36257,
            valueDate: '2023-06-14',
            price: 825.1,
            createdAt: '2023-06-14T22:00:00Z',
            changedAt: '2023-06-15T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-06-14',
                },
            ],
        },
        {
            id: 36226,
            valueDate: '2023-06-13',
            price: 827.1,
            createdAt: '2023-06-13T22:00:00Z',
            changedAt: '2023-06-13T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-06-13',
                },
            ],
        },
        {
            id: 36195,
            valueDate: '2023-06-12',
            price: 828.6,
            createdAt: '2023-06-12T22:00:00Z',
            changedAt: '2023-06-12T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-06-12',
                },
            ],
        },
        {
            id: 36159,
            valueDate: '2023-06-09',
            price: 829.1,
            createdAt: '2023-06-09T22:00:00Z',
            changedAt: '2023-06-11T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-06-09',
                },
            ],
        },
        {
            id: 36128,
            valueDate: '2023-06-08',
            price: 827.1,
            createdAt: '2023-06-08T22:00:00Z',
            changedAt: '2023-06-08T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-06-08',
                },
            ],
        },
        {
            id: 36097,
            valueDate: '2023-06-07',
            price: 828.8,
            createdAt: '2023-06-07T22:00:00Z',
            changedAt: '2023-06-07T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-06-07',
                },
            ],
        },
        {
            id: 36066,
            valueDate: '2023-06-06',
            price: 832.5,
            createdAt: '2023-06-06T22:00:00Z',
            changedAt: '2023-06-06T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-06-06',
                },
            ],
        },
        {
            id: 36035,
            valueDate: '2023-06-05',
            price: 832,
            createdAt: '2023-06-05T22:00:00Z',
            changedAt: '2023-06-05T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-06-05',
                },
            ],
        },
        {
            id: 35988,
            valueDate: '2023-06-02',
            price: 836.3,
            createdAt: '2023-06-02T22:00:00Z',
            changedAt: '2023-06-04T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-06-02',
                },
            ],
        },
        {
            id: 35957,
            valueDate: '2023-06-01',
            price: 836.6,
            createdAt: '2023-06-01T22:00:00Z',
            changedAt: '2023-06-01T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-06-01',
                },
            ],
        },
        {
            id: 35926,
            valueDate: '2023-05-31',
            price: 836.8,
            createdAt: '2023-05-31T22:00:00Z',
            changedAt: '2023-05-31T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-05-31',
                },
            ],
        },
        {
            id: 35895,
            valueDate: '2023-05-30',
            price: 832.4,
            createdAt: '2023-05-30T22:00:00Z',
            changedAt: '2023-05-30T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-05-30',
                },
            ],
        },
        {
            id: 35860,
            valueDate: '2023-05-26',
            price: 821.4,
            createdAt: '2023-05-26T22:00:00Z',
            changedAt: '2023-05-29T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-05-26',
                },
            ],
        },
        {
            id: 35806,
            valueDate: '2023-05-23',
            price: 825.8,
            createdAt: '2023-05-23T22:00:00Z',
            changedAt: '2023-05-25T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-05-23',
                },
            ],
        },
        {
            id: 35775,
            valueDate: '2023-05-22',
            price: 826.2,
            createdAt: '2023-05-22T22:00:00Z',
            changedAt: '2023-05-22T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-05-22',
                },
            ],
        },
        {
            id: 35727,
            valueDate: '2023-05-19',
            price: 827.7,
            createdAt: '2023-05-19T22:00:00Z',
            changedAt: '2023-05-21T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-05-19',
                },
            ],
        },
        {
            id: 35696,
            valueDate: '2023-05-17',
            price: 831.2,
            createdAt: '2023-05-17T22:00:00Z',
            changedAt: '2023-05-18T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-05-17',
                },
            ],
        },
        {
            id: 35665,
            valueDate: '2023-05-16',
            price: 828.7,
            createdAt: '2023-05-16T22:00:00Z',
            changedAt: '2023-05-16T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-05-16',
                },
            ],
        },
        {
            id: 35634,
            valueDate: '2023-05-15',
            price: 831.1,
            createdAt: '2023-05-15T22:00:00Z',
            changedAt: '2023-05-15T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-05-15',
                },
            ],
        },
        {
            id: 35599,
            valueDate: '2023-05-12',
            price: 831.1,
            createdAt: '2023-05-12T22:00:00Z',
            changedAt: '2023-05-14T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-05-12',
                },
            ],
        },
        {
            id: 35568,
            valueDate: '2023-05-11',
            price: 835.6,
            createdAt: '2023-05-11T22:00:00Z',
            changedAt: '2023-05-11T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-05-11',
                },
            ],
        },
        {
            id: 35537,
            valueDate: '2023-05-10',
            price: 830,
            createdAt: '2023-05-10T22:00:00Z',
            changedAt: '2023-05-10T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-05-10',
                },
            ],
        },
        {
            id: 35506,
            valueDate: '2023-05-09',
            price: 824.9,
            createdAt: '2023-05-09T22:00:00Z',
            changedAt: '2023-05-09T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-05-09',
                },
            ],
        },
        {
            id: 35475,
            valueDate: '2023-05-08',
            price: 826.5,
            createdAt: '2023-05-08T22:00:00Z',
            changedAt: '2023-05-08T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-05-08',
                },
            ],
        },
        {
            id: 35428,
            valueDate: '2023-05-05',
            price: 824.9,
            createdAt: '2023-05-05T22:00:00Z',
            changedAt: '2023-05-07T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-05-05',
                },
            ],
        },
        {
            id: 35397,
            valueDate: '2023-05-04',
            price: 829.1,
            createdAt: '2023-05-04T22:00:00Z',
            changedAt: '2023-05-04T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-05-04',
                },
            ],
        },
        {
            id: 35366,
            valueDate: '2023-05-03',
            price: 824.6,
            createdAt: '2023-05-03T22:00:00Z',
            changedAt: '2023-05-03T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-05-03',
                },
            ],
        },
        {
            id: 35335,
            valueDate: '2023-05-02',
            price: 827.5,
            createdAt: '2023-05-02T22:00:00Z',
            changedAt: '2023-05-02T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-05-02',
                },
            ],
        },
        {
            id: 35299,
            valueDate: '2023-04-28',
            price: 823.5,
            createdAt: '2023-04-28T22:00:00Z',
            changedAt: '2023-05-01T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-04-28',
                },
            ],
        },
        {
            id: 35268,
            valueDate: '2023-04-27',
            price: 819,
            createdAt: '2023-04-27T22:00:00Z',
            changedAt: '2023-04-27T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-04-27',
                },
            ],
        },
        {
            id: 35237,
            valueDate: '2023-04-26',
            price: 823.6,
            createdAt: '2023-04-26T22:00:00Z',
            changedAt: '2023-04-26T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-04-26',
                },
            ],
        },
        {
            id: 35206,
            valueDate: '2023-04-25',
            price: 822.9,
            createdAt: '2023-04-25T22:00:00Z',
            changedAt: '2023-04-25T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-04-25',
                },
            ],
        },
        {
            id: 35175,
            valueDate: '2023-04-24',
            price: 816.8,
            createdAt: '2023-04-24T22:00:00Z',
            changedAt: '2023-04-24T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-04-24',
                },
            ],
        },
        {
            id: 35128,
            valueDate: '2023-04-21',
            price: 818,
            createdAt: '2023-04-21T22:00:00Z',
            changedAt: '2023-04-23T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-04-21',
                },
            ],
        },
        {
            id: 35097,
            valueDate: '2023-04-20',
            price: 819,
            createdAt: '2023-04-20T22:00:00Z',
            changedAt: '2023-04-20T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-04-20',
                },
            ],
        },
        {
            id: 35066,
            valueDate: '2023-04-19',
            price: 814.9,
            createdAt: '2023-04-19T22:00:00Z',
            changedAt: '2023-04-19T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-04-19',
                },
            ],
        },
        {
            id: 35035,
            valueDate: '2023-04-18',
            price: 817.4,
            createdAt: '2023-04-18T22:00:00Z',
            changedAt: '2023-04-18T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-04-18',
                },
            ],
        },
        {
            id: 34997,
            valueDate: '2023-04-14',
            price: 819,
            createdAt: '2023-04-14T22:00:00Z',
            changedAt: '2023-04-16T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-04-14',
                },
            ],
        },
        {
            id: 34966,
            valueDate: '2023-04-13',
            price: 820.3,
            createdAt: '2023-04-13T22:00:00Z',
            changedAt: '2023-04-13T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-04-13',
                },
            ],
        },
        {
            id: 34935,
            valueDate: '2023-04-12',
            price: 819.5,
            createdAt: '2023-04-12T22:00:00Z',
            changedAt: '2023-04-12T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-04-12',
                },
            ],
        },
        {
            id: 34904,
            valueDate: '2023-04-06',
            price: 830.4,
            createdAt: '2023-04-06T22:00:00Z',
            changedAt: '2023-04-10T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-04-06',
                },
            ],
        },
        {
            id: 34874,
            valueDate: '2023-04-05',
            price: 826.1,
            createdAt: '2023-04-05T22:00:00Z',
            changedAt: '2023-04-05T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-04-05',
                },
            ],
        },
        {
            id: 34844,
            valueDate: '2023-04-04',
            price: 819.9,
            createdAt: '2023-04-04T22:00:00Z',
            changedAt: '2023-04-04T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-04-04',
                },
            ],
        },
        {
            id: 34814,
            valueDate: '2023-04-03',
            price: 822.2,
            createdAt: '2023-04-03T22:00:00Z',
            changedAt: '2023-04-03T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-04-03',
                },
            ],
        },
        {
            id: 34774,
            valueDate: '2023-03-31',
            price: 825.7,
            createdAt: '2023-03-31T22:00:00Z',
            changedAt: '2023-04-02T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-03-31',
                },
            ],
        },
        {
            id: 34744,
            valueDate: '2023-03-30',
            price: 816.3,
            createdAt: '2023-03-30T22:00:00Z',
            changedAt: '2023-03-30T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-03-30',
                },
            ],
        },
        {
            id: 34714,
            valueDate: '2023-03-29',
            price: 817.9,
            createdAt: '2023-03-29T22:00:00Z',
            changedAt: '2023-03-29T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-03-29',
                },
            ],
        },
        {
            id: 34684,
            valueDate: '2023-03-28',
            price: 819.9,
            createdAt: '2023-03-28T22:00:00Z',
            changedAt: '2023-03-28T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-03-28',
                },
            ],
        },
        {
            id: 34654,
            valueDate: '2023-03-27',
            price: 826.2,
            createdAt: '2023-03-27T22:00:00Z',
            changedAt: '2023-03-27T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-03-27',
                },
            ],
        },
        {
            id: 34624,
            valueDate: '2023-03-24',
            price: 829.3,
            createdAt: '2023-03-24T23:00:00Z',
            changedAt: '2023-03-26T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-03-24',
                },
            ],
        },
        {
            id: 34584,
            valueDate: '2023-03-22',
            price: 831.3,
            createdAt: '2023-03-22T23:00:00Z',
            changedAt: '2023-03-22T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-03-22',
                },
            ],
        },
        {
            id: 34554,
            valueDate: '2023-03-21',
            price: 832.1,
            createdAt: '2023-03-21T23:00:00Z',
            changedAt: '2023-03-21T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-03-21',
                },
            ],
        },
        {
            id: 34524,
            valueDate: '2023-03-20',
            price: 846,
            createdAt: '2023-03-20T23:00:00Z',
            changedAt: '2023-03-20T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-03-20',
                },
            ],
        },
        {
            id: 34484,
            valueDate: '2023-03-17',
            price: 848.8,
            createdAt: '2023-03-17T23:00:00Z',
            changedAt: '2023-03-19T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-03-17',
                },
            ],
        },
        {
            id: 34454,
            valueDate: '2023-03-16',
            price: 843.1,
            createdAt: '2023-03-16T23:00:00Z',
            changedAt: '2023-03-16T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-03-16',
                },
            ],
        },
        {
            id: 34424,
            valueDate: '2023-03-15',
            price: 849.2,
            createdAt: '2023-03-15T23:00:00Z',
            changedAt: '2023-03-15T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-03-15',
                },
            ],
        },
        {
            id: 34394,
            valueDate: '2023-03-14',
            price: 827.3,
            createdAt: '2023-03-14T23:00:00Z',
            changedAt: '2023-03-14T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-03-14',
                },
            ],
        },
        {
            id: 34364,
            valueDate: '2023-03-13',
            price: 834.1,
            createdAt: '2023-03-13T23:00:00Z',
            changedAt: '2023-03-13T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-03-13',
                },
            ],
        },
        {
            id: 34304,
            valueDate: '2023-03-10',
            price: 817.2,
            createdAt: '2023-03-10T23:00:00Z',
            changedAt: '2023-03-12T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-03-10',
                },
            ],
        },
        {
            id: 34264,
            valueDate: '2023-03-08',
            price: 815.9,
            createdAt: '2023-03-08T23:00:00Z',
            changedAt: '2023-03-09T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-03-08',
                },
            ],
        },
        {
            id: 34234,
            valueDate: '2023-03-07',
            price: 812.2,
            createdAt: '2023-03-07T23:00:00Z',
            changedAt: '2023-03-07T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-03-07',
                },
            ],
        },
        {
            id: 34204,
            valueDate: '2023-03-06',
            price: 811.2,
            createdAt: '2023-03-06T23:00:00Z',
            changedAt: '2023-03-06T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-03-06',
                },
            ],
        },
        {
            id: 34169,
            valueDate: '2023-03-03',
            price: 812.4,
            createdAt: '2023-03-03T23:00:00Z',
            changedAt: '2023-03-05T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-03-03',
                },
            ],
        },
        {
            id: 34138,
            valueDate: '2023-03-02',
            price: 811,
            createdAt: '2023-03-02T23:00:00Z',
            changedAt: '2023-03-02T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-03-02',
                },
            ],
        },
        {
            id: 34107,
            valueDate: '2023-03-01',
            price: 814.1,
            createdAt: '2023-03-01T23:00:00Z',
            changedAt: '2023-03-01T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-03-01',
                },
            ],
        },
        {
            id: 34076,
            valueDate: '2023-02-28',
            price: 819.2,
            createdAt: '2023-02-28T23:00:00Z',
            changedAt: '2023-02-28T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-02-28',
                },
            ],
        },
        {
            id: 34045,
            valueDate: '2023-02-27',
            price: 821,
            createdAt: '2023-02-27T23:00:00Z',
            changedAt: '2023-02-27T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-02-27',
                },
            ],
        },
        {
            id: 34009,
            valueDate: '2023-02-24',
            price: 821.2,
            createdAt: '2023-02-24T23:00:00Z',
            changedAt: '2023-02-26T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-02-24',
                },
            ],
        },
        {
            id: 33978,
            valueDate: '2023-02-23',
            price: 823.6,
            createdAt: '2023-02-23T23:00:00Z',
            changedAt: '2023-02-23T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-02-23',
                },
            ],
        },
        {
            id: 33947,
            valueDate: '2023-02-22',
            price: 824.2,
            createdAt: '2023-02-22T23:00:00Z',
            changedAt: '2023-02-22T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-02-22',
                },
            ],
        },
        {
            id: 33916,
            valueDate: '2023-02-21',
            price: 823.9,
            createdAt: '2023-02-21T23:00:00Z',
            changedAt: '2023-02-21T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-02-21',
                },
            ],
        },
        {
            id: 33885,
            valueDate: '2023-02-20',
            price: 828.4,
            createdAt: '2023-02-20T23:00:00Z',
            changedAt: '2023-02-20T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-02-20',
                },
            ],
        },
        {
            id: 33849,
            valueDate: '2023-02-17',
            price: 828,
            createdAt: '2023-02-17T23:00:00Z',
            changedAt: '2023-02-19T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-02-17',
                },
            ],
        },
        {
            id: 33818,
            valueDate: '2023-02-16',
            price: 827.8,
            createdAt: '2023-02-16T23:00:00Z',
            changedAt: '2023-02-16T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-02-16',
                },
            ],
        },
        {
            id: 33787,
            valueDate: '2023-02-15',
            price: 809.4,
            createdAt: '2023-02-15T23:00:00Z',
            changedAt: '2023-02-15T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-02-15',
                },
            ],
        },
        {
            id: 33756,
            valueDate: '2023-02-14',
            price: 804.2,
            createdAt: '2023-02-14T23:00:00Z',
            changedAt: '2023-02-14T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-02-14',
                },
            ],
        },
        {
            id: 33725,
            valueDate: '2023-02-13',
            price: 807,
            createdAt: '2023-02-13T23:00:00Z',
            changedAt: '2023-02-13T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-02-13',
                },
            ],
        },
        {
            id: 33687,
            valueDate: '2023-02-10',
            price: 807.5,
            createdAt: '2023-02-10T23:00:00Z',
            changedAt: '2023-02-12T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-02-10',
                },
            ],
        },
        {
            id: 33656,
            valueDate: '2023-02-09',
            price: 814.2,
            createdAt: '2023-02-09T23:00:00Z',
            changedAt: '2023-02-09T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-02-09',
                },
            ],
        },
        {
            id: 33625,
            valueDate: '2023-02-08',
            price: 814.5,
            createdAt: '2023-02-08T23:00:00Z',
            changedAt: '2023-02-08T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-02-08',
                },
            ],
        },
        {
            id: 33576,
            valueDate: '2023-02-07',
            price: 816.8,
            createdAt: '2023-02-07T23:00:00Z',
            changedAt: '2023-02-07T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-02-07',
                },
            ],
        },
        {
            id: 33545,
            valueDate: '2023-02-06',
            price: 820,
            createdAt: '2023-02-06T23:00:00Z',
            changedAt: '2023-02-06T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-02-06',
                },
            ],
        },
        {
            id: 33500,
            valueDate: '2023-02-03',
            price: 823,
            createdAt: '2023-02-03T23:00:00Z',
            changedAt: '2023-02-05T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-02-03',
                },
            ],
        },
        {
            id: 33469,
            valueDate: '2023-02-02',
            price: 832.7,
            createdAt: '2023-02-02T23:00:00Z',
            changedAt: '2023-02-02T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-02-02',
                },
            ],
        },
        {
            id: 33438,
            valueDate: '2023-02-01',
            price: 817.4,
            createdAt: '2023-02-01T23:00:00Z',
            changedAt: '2023-02-01T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-02-01',
                },
            ],
        },
        {
            id: 33407,
            valueDate: '2023-01-31',
            price: 818.6,
            createdAt: '2023-01-31T23:00:00Z',
            changedAt: '2023-01-31T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-01-31',
                },
            ],
        },
        {
            id: 33376,
            valueDate: '2023-01-30',
            price: 823.7,
            createdAt: '2023-01-30T23:00:00Z',
            changedAt: '2023-01-30T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-01-30',
                },
            ],
        },
        {
            id: 33345,
            valueDate: '2023-01-27',
            price: 827.3,
            createdAt: '2023-01-29T23:00:00Z',
            changedAt: '2023-01-29T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-01-27',
                },
            ],
        },
        {
            id: 33298,
            valueDate: '2023-01-26',
            price: 824,
            createdAt: '2023-01-26T23:00:00Z',
            changedAt: '2023-01-26T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-01-26',
                },
            ],
        },
        {
            id: 33267,
            valueDate: '2023-01-25',
            price: 825.4,
            createdAt: '2023-01-25T23:00:00Z',
            changedAt: '2023-01-25T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-01-25',
                },
            ],
        },
        {
            id: 33236,
            valueDate: '2023-01-24',
            price: 826.2,
            createdAt: '2023-01-24T23:00:00Z',
            changedAt: '2023-01-24T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-01-24',
                },
            ],
        },
        {
            id: 33205,
            valueDate: '2023-01-23',
            price: 822.2,
            createdAt: '2023-01-23T23:00:00Z',
            changedAt: '2023-01-23T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-01-23',
                },
            ],
        },
        {
            id: 33169,
            valueDate: '2023-01-20',
            price: 825.4,
            createdAt: '2023-01-20T23:00:00Z',
            changedAt: '2023-01-22T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-01-20',
                },
            ],
        },
        {
            id: 33138,
            valueDate: '2023-01-19',
            price: 833.1,
            createdAt: '2023-01-19T23:00:00Z',
            changedAt: '2023-01-19T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-01-19',
                },
            ],
        },
        {
            id: 33107,
            valueDate: '2023-01-18',
            price: 837.5,
            createdAt: '2023-01-18T23:00:00Z',
            changedAt: '2023-01-18T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-01-18',
                },
            ],
        },
        {
            id: 33076,
            valueDate: '2023-01-17',
            price: 836.4,
            createdAt: '2023-01-17T23:00:00Z',
            changedAt: '2023-01-17T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-01-17',
                },
            ],
        },
        {
            id: 33045,
            valueDate: '2023-01-16',
            price: 846.6,
            createdAt: '2023-01-16T23:00:00Z',
            changedAt: '2023-01-16T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-01-16',
                },
            ],
        },
        {
            id: 33009,
            valueDate: '2023-01-13',
            price: 829.6,
            createdAt: '2023-01-13T23:00:00Z',
            changedAt: '2023-01-15T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-01-13',
                },
            ],
        },
        {
            id: 32978,
            valueDate: '2023-01-12',
            price: 828.7,
            createdAt: '2023-01-12T23:00:00Z',
            changedAt: '2023-01-12T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-01-12',
                },
            ],
        },
        {
            id: 32947,
            valueDate: '2023-01-11',
            price: 820.2,
            createdAt: '2023-01-11T23:00:00Z',
            changedAt: '2023-01-11T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-01-11',
                },
            ],
        },
        {
            id: 32916,
            valueDate: '2023-01-10',
            price: 811.8,
            createdAt: '2023-01-10T23:00:00Z',
            changedAt: '2023-01-10T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-01-10',
                },
            ],
        },
        {
            id: 32885,
            valueDate: '2023-01-09',
            price: 816.9,
            createdAt: '2023-01-09T23:00:00Z',
            changedAt: '2023-01-09T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-01-09',
                },
            ],
        },
        {
            id: 32844,
            valueDate: '2023-01-06',
            price: 818.8,
            createdAt: '2023-01-06T23:00:00Z',
            changedAt: '2023-01-08T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-01-06',
                },
            ],
        },
        {
            id: 32814,
            valueDate: '2023-01-05',
            price: 807,
            createdAt: '2023-01-05T23:00:00Z',
            changedAt: '2023-01-05T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-01-05',
                },
            ],
        },
        {
            id: 32784,
            valueDate: '2023-01-04',
            price: 824,
            createdAt: '2023-01-04T23:00:00Z',
            changedAt: '2023-01-04T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-01-04',
                },
            ],
        },
        {
            id: 32754,
            valueDate: '2023-01-03',
            price: 815.1,
            createdAt: '2023-01-03T23:00:00Z',
            changedAt: '2023-01-03T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2023-01-03',
                },
            ],
        },
        {
            id: 32724,
            valueDate: '2022-12-30',
            price: 788.2,
            createdAt: '2023-01-02T23:00:00Z',
            changedAt: '2023-01-02T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-12-30',
                },
            ],
        },
        {
            id: 32684,
            valueDate: '2022-12-29',
            price: 795.8,
            createdAt: '2022-12-29T23:00:00Z',
            changedAt: '2022-12-29T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-12-29',
                },
            ],
        },
        {
            id: 32654,
            valueDate: '2022-12-28',
            price: 792,
            createdAt: '2022-12-28T23:00:00Z',
            changedAt: '2022-12-28T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-12-28',
                },
            ],
        },
        {
            id: 32624,
            valueDate: '2022-12-27',
            price: 795.7,
            createdAt: '2022-12-27T23:00:00Z',
            changedAt: '2022-12-27T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-12-27',
                },
            ],
        },
        {
            id: 32584,
            valueDate: '2022-12-23',
            price: 806.1,
            createdAt: '2022-12-23T23:00:00Z',
            changedAt: '2022-12-26T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-12-23',
                },
            ],
        },
        {
            id: 32554,
            valueDate: '2022-12-22',
            price: 809.7,
            createdAt: '2022-12-22T23:00:00Z',
            changedAt: '2022-12-22T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-12-22',
                },
            ],
        },
        {
            id: 32524,
            valueDate: '2022-12-21',
            price: 813.8,
            createdAt: '2022-12-21T23:00:00Z',
            changedAt: '2022-12-21T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-12-21',
                },
            ],
        },
        {
            id: 32494,
            valueDate: '2022-12-20',
            price: 818.8,
            createdAt: '2022-12-20T23:00:00Z',
            changedAt: '2022-12-20T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-12-20',
                },
            ],
        },
        {
            id: 32464,
            valueDate: '2022-12-19',
            price: 819.1,
            createdAt: '2022-12-19T23:00:00Z',
            changedAt: '2022-12-19T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-12-19',
                },
            ],
        },
        {
            id: 32415,
            valueDate: '2022-12-16',
            price: 824.8,
            createdAt: '2022-12-16T23:00:00Z',
            changedAt: '2022-12-18T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-12-16',
                },
            ],
        },
        {
            id: 32384,
            valueDate: '2022-12-15',
            price: 825.9,
            createdAt: '2022-12-15T23:00:00Z',
            changedAt: '2022-12-15T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-12-15',
                },
            ],
        },
        {
            id: 32344,
            valueDate: '2022-12-14',
            price: 834.2,
            createdAt: '2022-12-14T23:00:00Z',
            changedAt: '2022-12-14T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-12-14',
                },
            ],
        },
        {
            id: 32314,
            valueDate: '2022-12-13',
            price: 832.3,
            createdAt: '2022-12-13T23:00:00Z',
            changedAt: '2022-12-13T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-12-13',
                },
            ],
        },
        {
            id: 32284,
            valueDate: '2022-12-12',
            price: 829.3,
            createdAt: '2022-12-12T23:00:00Z',
            changedAt: '2022-12-12T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-12-12',
                },
            ],
        },
        {
            id: 32244,
            valueDate: '2022-12-09',
            price: 832.2,
            createdAt: '2022-12-09T23:00:00Z',
            changedAt: '2022-12-11T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-12-09',
                },
            ],
        },
        {
            id: 32214,
            valueDate: '2022-12-08',
            price: 836.8,
            createdAt: '2022-12-08T23:00:00Z',
            changedAt: '2022-12-08T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-12-08',
                },
            ],
        },
        {
            id: 32184,
            valueDate: '2022-12-07',
            price: 836.3,
            createdAt: '2022-12-07T23:00:00Z',
            changedAt: '2022-12-07T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-12-07',
                },
            ],
        },
        {
            id: 32154,
            valueDate: '2022-12-06',
            price: 836.4,
            createdAt: '2022-12-06T23:00:00Z',
            changedAt: '2022-12-06T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-12-06',
                },
            ],
        },
        {
            id: 32124,
            valueDate: '2022-12-05',
            price: 829.4,
            createdAt: '2022-12-05T23:00:00Z',
            changedAt: '2022-12-05T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-12-05',
                },
            ],
        },
        {
            id: 32084,
            valueDate: '2022-12-02',
            price: 832,
            createdAt: '2022-12-02T23:00:00Z',
            changedAt: '2022-12-04T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-12-02',
                },
            ],
        },
        {
            id: 32054,
            valueDate: '2022-12-01',
            price: 832.3,
            createdAt: '2022-12-01T23:00:00Z',
            changedAt: '2022-12-01T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-12-01',
                },
            ],
        },
        {
            id: 32024,
            valueDate: '2022-11-30',
            price: 823.9,
            createdAt: '2022-11-30T23:00:00Z',
            changedAt: '2022-11-30T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-11-30',
                },
            ],
        },
        {
            id: 31984,
            valueDate: '2022-11-29',
            price: 826.1,
            createdAt: '2022-11-29T23:00:00Z',
            changedAt: '2022-11-29T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-11-29',
                },
            ],
        },
        {
            id: 31954,
            valueDate: '2022-11-28',
            price: 828.3,
            createdAt: '2022-11-28T23:00:00Z',
            changedAt: '2022-11-28T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-11-28',
                },
            ],
        },
        {
            id: 31924,
            valueDate: '2022-11-25',
            price: 816.4,
            createdAt: '2022-11-27T23:00:00Z',
            changedAt: '2022-11-27T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-11-25',
                },
            ],
        },
        {
            id: 31884,
            valueDate: '2022-11-24',
            price: 826,
            createdAt: '2022-11-24T23:00:00Z',
            changedAt: '2022-11-24T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-11-24',
                },
            ],
        },
        {
            id: 31844,
            valueDate: '2022-11-23',
            price: 821.9,
            createdAt: '2022-11-23T23:00:00Z',
            changedAt: '2022-11-23T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-11-23',
                },
            ],
        },
        {
            id: 31814,
            valueDate: '2022-11-22',
            price: 818.3,
            createdAt: '2022-11-22T23:00:00Z',
            changedAt: '2022-11-22T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-11-22',
                },
            ],
        },
        {
            id: 31784,
            valueDate: '2022-11-21',
            price: 817.7,
            createdAt: '2022-11-21T23:00:00Z',
            changedAt: '2022-11-21T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-11-21',
                },
            ],
        },
        {
            id: 31744,
            valueDate: '2022-11-18',
            price: 820.7,
            createdAt: '2022-11-18T23:00:00Z',
            changedAt: '2022-11-20T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-11-18',
                },
            ],
        },
        {
            id: 31714,
            valueDate: '2022-11-17',
            price: 810.5,
            createdAt: '2022-11-17T23:00:00Z',
            changedAt: '2022-11-17T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-11-17',
                },
            ],
        },
        {
            id: 31684,
            valueDate: '2022-11-16',
            price: 812.8,
            createdAt: '2022-11-16T23:00:00Z',
            changedAt: '2022-11-16T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-11-16',
                },
            ],
        },
        {
            id: 31656,
            valueDate: '2022-11-14',
            price: 801.2,
            createdAt: '2022-11-14T23:00:00Z',
            changedAt: '2022-11-15T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-11-14',
                },
            ],
        },
        {
            id: 31616,
            valueDate: '2022-11-11',
            price: 801,
            createdAt: '2022-11-11T23:00:00Z',
            changedAt: '2022-11-13T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-11-11',
                },
            ],
        },
        {
            id: 31586,
            valueDate: '2022-11-10',
            price: 795.3,
            createdAt: '2022-11-10T23:00:00Z',
            changedAt: '2022-11-10T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-11-10',
                },
            ],
        },
        {
            id: 31556,
            valueDate: '2022-11-09',
            price: 798.2,
            createdAt: '2022-11-09T23:00:00Z',
            changedAt: '2022-11-09T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-11-09',
                },
            ],
        },
        {
            id: 31526,
            valueDate: '2022-11-08',
            price: 793.5,
            createdAt: '2022-11-08T23:00:00Z',
            changedAt: '2022-11-08T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-11-08',
                },
            ],
        },
        {
            id: 31496,
            valueDate: '2022-11-07',
            price: 786.1,
            createdAt: '2022-11-07T23:00:00Z',
            changedAt: '2022-11-07T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-11-07',
                },
            ],
        },
        {
            id: 31466,
            valueDate: '2022-11-04',
            price: 784.1,
            createdAt: '2022-11-04T23:00:00Z',
            changedAt: '2022-11-06T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-11-04',
                },
            ],
        },
        {
            id: 31436,
            valueDate: '2022-11-03',
            price: 783.2,
            createdAt: '2022-11-03T23:00:00Z',
            changedAt: '2022-11-03T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-11-03',
                },
            ],
        },
        {
            id: 31396,
            valueDate: '2022-11-02',
            price: 793.5,
            createdAt: '2022-11-02T23:00:00Z',
            changedAt: '2022-11-02T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-11-02',
                },
            ],
        },
        {
            id: 31366,
            valueDate: '2022-11-01',
            price: 784.8,
            createdAt: '2022-11-01T23:00:00Z',
            changedAt: '2022-11-01T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-11-01',
                },
            ],
        },
        {
            id: 31336,
            valueDate: '2022-10-31',
            price: 792.3,
            createdAt: '2022-10-31T23:00:00Z',
            changedAt: '2022-10-31T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-10-31',
                },
            ],
        },
        {
            id: 31296,
            valueDate: '2022-10-28',
            price: 796.9,
            createdAt: '2022-10-28T22:00:00Z',
            changedAt: '2022-10-30T23:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-10-28',
                },
            ],
        },
        {
            id: 31266,
            valueDate: '2022-10-27',
            price: 800.2,
            createdAt: '2022-10-27T22:00:00Z',
            changedAt: '2022-10-27T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-10-27',
                },
            ],
        },
        {
            id: 31236,
            valueDate: '2022-10-26',
            price: 788.7,
            createdAt: '2022-10-26T22:00:00Z',
            changedAt: '2022-10-26T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-10-26',
                },
            ],
        },
        {
            id: 31206,
            valueDate: '2022-10-25',
            price: 789.4,
            createdAt: '2022-10-25T22:00:00Z',
            changedAt: '2022-10-25T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-10-25',
                },
            ],
        },
        {
            id: 31176,
            valueDate: '2022-10-24',
            price: 778.6,
            createdAt: '2022-10-24T22:00:00Z',
            changedAt: '2022-10-24T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-10-24',
                },
            ],
        },
        {
            id: 31146,
            valueDate: '2022-10-21',
            price: 776.1,
            createdAt: '2022-10-21T22:00:00Z',
            changedAt: '2022-10-23T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-10-21',
                },
            ],
        },
        {
            id: 31116,
            valueDate: '2022-10-20',
            price: 778.7,
            createdAt: '2022-10-20T22:00:00Z',
            changedAt: '2022-10-20T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-10-20',
                },
            ],
        },
        {
            id: 31075,
            valueDate: '2022-10-19',
            price: 781,
            createdAt: '2022-10-19T22:00:00Z',
            changedAt: '2022-10-19T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-10-19',
                },
            ],
        },
        {
            id: 31045,
            valueDate: '2022-10-18',
            price: 786.7,
            createdAt: '2022-10-18T22:00:00Z',
            changedAt: '2022-10-18T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-10-18',
                },
            ],
        },
        {
            id: 31015,
            valueDate: '2022-10-17',
            price: 780.3,
            createdAt: '2022-10-17T22:00:00Z',
            changedAt: '2022-10-17T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-10-17',
                },
            ],
        },
        {
            id: 30977,
            valueDate: '2022-10-14',
            price: 766.4,
            createdAt: '2022-10-14T22:00:00Z',
            changedAt: '2022-10-16T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-10-14',
                },
            ],
        },
        {
            id: 30947,
            valueDate: '2022-10-13',
            price: 770.6,
            createdAt: '2022-10-13T22:00:00Z',
            changedAt: '2022-10-13T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-10-13',
                },
            ],
        },
        {
            id: 30917,
            valueDate: '2022-10-12',
            price: 770.8,
            createdAt: '2022-10-12T22:00:00Z',
            changedAt: '2022-10-12T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-10-12',
                },
            ],
        },
        {
            id: 30887,
            valueDate: '2022-10-11',
            price: 764,
            createdAt: '2022-10-11T22:00:00Z',
            changedAt: '2022-10-11T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-10-11',
                },
            ],
        },
        {
            id: 30859,
            valueDate: '2022-10-10',
            price: 762.8,
            createdAt: '2022-10-10T22:00:00Z',
            changedAt: '2022-10-10T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-10-10',
                },
            ],
        },
        {
            id: 30833,
            valueDate: '2022-10-07',
            price: 775.7,
            createdAt: '2022-10-07T22:00:00Z',
            changedAt: '2022-10-09T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-10-07',
                },
            ],
        },
        {
            id: 30803,
            valueDate: '2022-10-06',
            price: 782.3,
            createdAt: '2022-10-06T22:00:00Z',
            changedAt: '2022-10-06T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-10-06',
                },
            ],
        },
        {
            id: 30773,
            valueDate: '2022-10-05',
            price: 790.9,
            createdAt: '2022-10-05T22:00:00Z',
            changedAt: '2022-10-05T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-10-05',
                },
            ],
        },
        {
            id: 30744,
            valueDate: '2022-10-04',
            price: 807.4,
            createdAt: '2022-10-04T22:00:00Z',
            changedAt: '2022-10-04T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-10-04',
                },
            ],
        },
        {
            id: 30715,
            valueDate: '2022-10-03',
            price: 800.4,
            createdAt: '2022-10-03T22:00:00Z',
            changedAt: '2022-10-03T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-10-03',
                },
            ],
        },
        {
            id: 30675,
            valueDate: '2022-09-30',
            price: 784.4,
            createdAt: '2022-09-30T22:00:00Z',
            changedAt: '2022-10-02T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-09-30',
                },
            ],
        },
        {
            id: 30631,
            valueDate: '2022-09-29',
            price: 786.8,
            createdAt: '2022-09-29T22:00:00Z',
            changedAt: '2022-09-29T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-09-29',
                },
            ],
        },
        {
            id: 30602,
            valueDate: '2022-09-28',
            price: 787.5,
            createdAt: '2022-09-28T22:00:00Z',
            changedAt: '2022-09-28T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-09-28',
                },
            ],
        },
        {
            id: 30573,
            valueDate: '2022-09-27',
            price: 774.5,
            createdAt: '2022-09-27T22:00:00Z',
            changedAt: '2022-09-27T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-09-27',
                },
            ],
        },
        {
            id: 30544,
            valueDate: '2022-09-26',
            price: 783.8,
            createdAt: '2022-09-26T22:00:00Z',
            changedAt: '2022-09-26T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-09-26',
                },
            ],
        },
        {
            id: 30515,
            valueDate: '2022-09-23',
            price: 787.5,
            createdAt: '2022-09-23T22:00:00Z',
            changedAt: '2022-09-25T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-09-23',
                },
            ],
        },
        {
            id: 30486,
            valueDate: '2022-09-21',
            price: 800.6,
            createdAt: '2022-09-21T22:00:00Z',
            changedAt: '2022-09-21T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-09-21',
                },
            ],
        },
        {
            id: 30438,
            valueDate: '2022-09-20',
            price: 798.4,
            createdAt: '2022-09-20T22:00:00Z',
            changedAt: '2022-09-20T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-09-20',
                },
            ],
        },
        {
            id: 30407,
            valueDate: '2022-09-19',
            price: 805.8,
            createdAt: '2022-09-19T22:00:00Z',
            changedAt: '2022-09-19T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-09-19',
                },
            ],
        },
        {
            id: 30371,
            valueDate: '2022-09-16',
            price: 806.4,
            createdAt: '2022-09-16T22:00:00Z',
            changedAt: '2022-09-18T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-09-16',
                },
            ],
        },
        {
            id: 30340,
            valueDate: '2022-09-15',
            price: 810.3,
            createdAt: '2022-09-15T22:00:00Z',
            changedAt: '2022-09-15T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-09-15',
                },
            ],
        },
        {
            id: 30309,
            valueDate: '2022-09-14',
            price: 812,
            createdAt: '2022-09-14T22:00:00Z',
            changedAt: '2022-09-14T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-09-14',
                },
            ],
        },
        {
            id: 30278,
            valueDate: '2022-09-13',
            price: 807.5,
            createdAt: '2022-09-13T22:00:00Z',
            changedAt: '2022-09-13T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-09-13',
                },
            ],
        },
        {
            id: 30247,
            valueDate: '2022-09-12',
            price: 818.3,
            createdAt: '2022-09-12T22:00:00Z',
            changedAt: '2022-09-12T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-09-12',
                },
            ],
        },
        {
            id: 30207,
            valueDate: '2022-09-09',
            price: 815,
            createdAt: '2022-09-09T22:00:00Z',
            changedAt: '2022-09-11T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-09-09',
                },
            ],
        },
        {
            id: 30160,
            valueDate: '2022-09-08',
            price: 817.6,
            createdAt: '2022-09-08T22:00:00Z',
            changedAt: '2022-09-08T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-09-08',
                },
            ],
        },
        {
            id: 30129,
            valueDate: '2022-09-07',
            price: 821.9,
            createdAt: '2022-09-07T22:00:00Z',
            changedAt: '2022-09-07T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-09-07',
                },
            ],
        },
        {
            id: 30098,
            valueDate: '2022-09-06',
            price: 821.6,
            createdAt: '2022-09-06T22:00:00Z',
            changedAt: '2022-09-06T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-09-06',
                },
            ],
        },
        {
            id: 30067,
            valueDate: '2022-09-05',
            price: 818.6,
            createdAt: '2022-09-05T22:00:00Z',
            changedAt: '2022-09-05T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-09-05',
                },
            ],
        },
        {
            id: 30027,
            valueDate: '2022-09-02',
            price: 824,
            createdAt: '2022-09-02T22:00:00Z',
            changedAt: '2022-09-04T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-09-02',
                },
            ],
        },
        {
            id: 29978,
            valueDate: '2022-09-01',
            price: 820.4,
            createdAt: '2022-09-01T22:00:00Z',
            changedAt: '2022-09-01T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-09-01',
                },
            ],
        },
        {
            id: 29947,
            valueDate: '2022-08-31',
            price: 825,
            createdAt: '2022-08-31T22:00:00Z',
            changedAt: '2022-08-31T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-08-31',
                },
            ],
        },
        {
            id: 29908,
            valueDate: '2022-08-30',
            price: 824.1,
            createdAt: '2022-08-30T22:00:00Z',
            changedAt: '2022-08-30T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-08-30',
                },
            ],
        },
        {
            id: 29876,
            valueDate: '2022-08-29',
            price: 821.4,
            createdAt: '2022-08-29T22:00:00Z',
            changedAt: '2022-08-29T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-08-29',
                },
            ],
        },
        {
            id: 29846,
            valueDate: '2022-08-26',
            price: 822,
            createdAt: '2022-08-28T22:00:00Z',
            changedAt: '2022-08-28T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-08-26',
                },
            ],
        },
        {
            id: 29806,
            valueDate: '2022-08-24',
            price: 824.7,
            createdAt: '2022-08-24T22:00:00Z',
            changedAt: '2022-08-24T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-08-24',
                },
            ],
        },
        {
            id: 29776,
            valueDate: '2022-08-23',
            price: 828.2,
            createdAt: '2022-08-23T22:00:00Z',
            changedAt: '2022-08-23T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-08-23',
                },
            ],
        },
        {
            id: 29746,
            valueDate: '2022-08-22',
            price: 834.4,
            createdAt: '2022-08-22T22:00:00Z',
            changedAt: '2022-08-22T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-08-22',
                },
            ],
        },
        {
            id: 29706,
            valueDate: '2022-08-19',
            price: 837.3,
            createdAt: '2022-08-19T22:00:00Z',
            changedAt: '2022-08-21T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-08-19',
                },
            ],
        },
        {
            id: 29676,
            valueDate: '2022-08-18',
            price: 850.1,
            createdAt: '2022-08-18T22:00:00Z',
            changedAt: '2022-08-18T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-08-18',
                },
            ],
        },
        {
            id: 29646,
            valueDate: '2022-08-17',
            price: 850.2,
            createdAt: '2022-08-17T22:00:00Z',
            changedAt: '2022-08-17T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-08-17',
                },
            ],
        },
        {
            id: 29616,
            valueDate: '2022-08-16',
            price: 859,
            createdAt: '2022-08-16T22:00:00Z',
            changedAt: '2022-08-16T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-08-16',
                },
            ],
        },
        {
            id: 29586,
            valueDate: '2022-08-15',
            price: 865.8,
            createdAt: '2022-08-15T22:00:00Z',
            changedAt: '2022-08-15T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-08-15',
                },
            ],
        },
        {
            id: 29546,
            valueDate: '2022-08-12',
            price: 857,
            createdAt: '2022-08-14T22:00:00Z',
            changedAt: '2022-08-14T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-08-12',
                },
            ],
        },
        {
            id: 29516,
            valueDate: '2022-08-11',
            price: 856.3,
            createdAt: '2022-08-11T22:00:00Z',
            changedAt: '2022-08-11T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-08-11',
                },
            ],
        },
        {
            id: 29486,
            valueDate: '2022-08-10',
            price: 863.4,
            createdAt: '2022-08-10T22:00:00Z',
            changedAt: '2022-08-10T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-08-10',
                },
            ],
        },
        {
            id: 29456,
            valueDate: '2022-08-09',
            price: 856.9,
            createdAt: '2022-08-09T22:00:00Z',
            changedAt: '2022-08-09T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-08-09',
                },
            ],
        },
        {
            id: 29426,
            valueDate: '2022-08-08',
            price: 860.1,
            createdAt: '2022-08-08T22:00:00Z',
            changedAt: '2022-08-08T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-08-08',
                },
            ],
        },
        {
            id: 29386,
            valueDate: '2022-08-05',
            price: 854.3,
            createdAt: '2022-08-05T22:00:00Z',
            changedAt: '2022-08-07T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-08-05',
                },
            ],
        },
        {
            id: 29349,
            valueDate: '2022-08-04',
            price: 872.4,
            createdAt: '2022-08-04T22:00:00Z',
            changedAt: '2022-08-04T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-08-04',
                },
            ],
        },
        {
            id: 29318,
            valueDate: '2022-08-03',
            price: 867.5,
            createdAt: '2022-08-03T22:00:00Z',
            changedAt: '2022-08-03T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-08-03',
                },
            ],
        },
        {
            id: 29287,
            valueDate: '2022-08-02',
            price: 858.3,
            createdAt: '2022-08-02T22:00:00Z',
            changedAt: '2022-08-02T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-08-02',
                },
            ],
        },
        {
            id: 29249,
            valueDate: '2022-07-29',
            price: 863.3,
            createdAt: '2022-07-29T22:00:00Z',
            changedAt: '2022-08-01T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-07-29',
                },
            ],
        },
        {
            id: 29218,
            valueDate: '2022-07-28',
            price: 863.7,
            createdAt: '2022-07-28T22:00:00Z',
            changedAt: '2022-07-28T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-07-28',
                },
            ],
        },
        {
            id: 29187,
            valueDate: '2022-07-27',
            price: 856.5,
            createdAt: '2022-07-27T22:00:00Z',
            changedAt: '2022-07-27T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-07-27',
                },
            ],
        },
        {
            id: 29138,
            valueDate: '2022-07-26',
            price: 853.9,
            createdAt: '2022-07-26T22:00:00Z',
            changedAt: '2022-07-26T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-07-26',
                },
            ],
        },
        {
            id: 29107,
            valueDate: '2022-07-25',
            price: 846.9,
            createdAt: '2022-07-25T22:00:00Z',
            changedAt: '2022-07-25T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-07-25',
                },
            ],
        },
        {
            id: 29058,
            valueDate: '2022-07-22',
            price: 838.5,
            createdAt: '2022-07-22T22:00:00Z',
            changedAt: '2022-07-24T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-07-22',
                },
            ],
        },
        {
            id: 29027,
            valueDate: '2022-07-21',
            price: 837.4,
            createdAt: '2022-07-21T22:00:00Z',
            changedAt: '2022-07-21T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-07-21',
                },
            ],
        },
        {
            id: 28987,
            valueDate: '2022-07-19',
            price: 838.5,
            createdAt: '2022-07-19T22:00:00Z',
            changedAt: '2022-07-20T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-07-19',
                },
            ],
        },
        {
            id: 28947,
            valueDate: '2022-07-18',
            price: 834.4,
            createdAt: '2022-07-18T22:00:00Z',
            changedAt: '2022-07-18T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-07-18',
                },
            ],
        },
        {
            id: 28907,
            valueDate: '2022-07-15',
            price: 831.5,
            createdAt: '2022-07-15T22:00:00Z',
            changedAt: '2022-07-17T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-07-15',
                },
            ],
        },
        {
            id: 28867,
            valueDate: '2022-07-14',
            price: 839.5,
            createdAt: '2022-07-14T22:00:00Z',
            changedAt: '2022-07-14T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-07-14',
                },
            ],
        },
        {
            id: 28818,
            valueDate: '2022-07-13',
            price: 832.9,
            createdAt: '2022-07-13T22:00:00Z',
            changedAt: '2022-07-13T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-07-13',
                },
            ],
        },
        {
            id: 28787,
            valueDate: '2022-07-12',
            price: 838.9,
            createdAt: '2022-07-12T22:00:00Z',
            changedAt: '2022-07-12T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-07-12',
                },
            ],
        },
        {
            id: 28746,
            valueDate: '2022-07-11',
            price: 834.9,
            createdAt: '2022-07-11T22:00:00Z',
            changedAt: '2022-07-11T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-07-11',
                },
            ],
        },
        {
            id: 28710,
            valueDate: '2022-07-08',
            price: 832.7,
            createdAt: '2022-07-08T22:00:00Z',
            changedAt: '2022-07-10T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-07-08',
                },
            ],
        },
        {
            id: 28679,
            valueDate: '2022-07-07',
            price: 838.8,
            createdAt: '2022-07-07T22:00:00Z',
            changedAt: '2022-07-07T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-07-07',
                },
            ],
        },
        {
            id: 28648,
            valueDate: '2022-07-06',
            price: 832.6,
            createdAt: '2022-07-06T22:00:00Z',
            changedAt: '2022-07-06T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-07-06',
                },
            ],
        },
        {
            id: 28617,
            valueDate: '2022-07-05',
            price: 814.9,
            createdAt: '2022-07-05T22:00:00Z',
            changedAt: '2022-07-05T22:00:00Z',
            links: [
                {
                    rel: 'certificate',
                    href: 'https://b2e-portal-v2-preprod.imobi.mobicorp.ch/vvl/finanzinstrumenten/rest/api/v1/certificates/DE000A3GWZE2?price.date=2022-07-05',
                },
            ],
        },
    ];
}
