import { HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';

import { Matchers, Pact } from '@pact-foundation/pact';

import { pactWith } from 'jest-pact';

import { lastValueFrom } from 'rxjs';

import { configureBaseURL } from '@mobi/rwc-utils-ng-jslib/testing';

import { DummyService, User } from './dummy.service';

pactWith(
    {
        consumer: 'vvl-finanzinstrumenten-poc-b2e-rwc',
        provider: 'backend-tk-name-id',
        spec: 3,
    },
    (provider: Pact) => {
        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [HttpClientModule],
                providers: [...configureBaseURL(provider.mockService.baseUrl), DummyService],
            });
        });

        let testee: DummyService;
        beforeEach(() => {
            testee = TestBed.inject(DummyService);
        });

        const userId = 1;

        const expectedUser: User = {
            id: 1,
            firstName: 'Toto',
            lastName: 'Titi',
        };

        it('should get user with id 1', async () => {
            await provider.addInteraction({
                state: `user 1 exists`,
                uponReceiving: 'a request to GET a user',
                withRequest: {
                    method: 'GET',
                    path: `/api/dummy/${userId}`,
                },
                willRespondWith: {
                    status: 200,
                    body: {
                        id: expectedUser.id,
                        firstName: Matchers.string(expectedUser.firstName),
                        lastName: Matchers.string(expectedUser.lastName),
                    },
                },
            });

            const response = await lastValueFrom(testee.get(userId));
            expect(response).toEqual(expectedUser);
        });
    },
);
