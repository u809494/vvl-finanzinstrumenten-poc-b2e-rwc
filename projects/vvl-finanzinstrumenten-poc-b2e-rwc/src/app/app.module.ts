import { NgModule } from '@angular/core';

import { DatePipe } from '@angular/common';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';

@NgModule({
    declarations: [AppComponent],
    imports: [AppRoutingModule, CoreModule],
    providers: [DatePipe],
    bootstrap: [AppComponent],
})
export class AppModule {}
