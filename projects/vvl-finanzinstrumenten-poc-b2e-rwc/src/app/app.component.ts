import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

import { TranslationService } from '@mobi/rwc-utils-ng-jslib';

import translations from './app.translation.json';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent implements OnInit {
    constructor(private translationService: TranslationService) {}

    ngOnInit(): void {
        this.translationService.addTranslation(translations);
    }
}
