import { DevTools } from '@mobi/rwc-utils-devtools-jslib';

module.exports = DevTools.create('vvl-finanzinstrumenten-poc-b2e-rwc', __dirname).build();
